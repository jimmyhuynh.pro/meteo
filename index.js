const api = '1878091f775474d97e3fc5c203fb4cfd'

let apicall = function(city) {

let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${api}&units=metric&lang=fr`;

fetch(url)
    .then((resp) => resp.json()
    .then((data => { console.log(data);
    
    document.querySelector('#city').innerHTML ="<i class='fas fa-city fa-2x'></i>" + data.name;
    document.querySelector('#temperature').innerHTML ="<i class='fas fa-thermometer-half fa-2x'></i>" + 'Temp. ress ' + data.main.feels_like+ '°';
    document.querySelector('#temperature-min').innerHTML ="<i class='fas fa-thermometer-quarter fa-2x'></i>"+'Temp. min ' + data.main.temp_min + '°';
    document.querySelector('#temperature-max').innerHTML ="<i class='fas fa-thermometer-three-quarters fa-2x'></i>"+'Temp. max ' + data.main.temp_max + '°';
    document.querySelector('#wind').innerHTML ="<i class='fas fa-wind fa-2x'></i>"+'Vent ' + Math.round(data.wind.speed*3.6) + 'km/h';
    document.querySelector('#wind-deg').innerHTML = "<i class='fas fa-arrow-alt-circle-up fa-2x' style = 'transform: rotate("+data.wind.deg+"deg"+")'></i>"+ "Dir. Vent";
    document.querySelector('#humidity').innerHTML ="<i class='fas fa-tint fa-2x'></i>"+'Humidité ' + data.main.humidity + '%';
    document.querySelector('#weather-icon').innerHTML = `<img src='http://openweathermap.org/img/wn/${data.weather[0].icon}.png'>`;
    })

)
.catch((error) => console.log('erreur : ' + error)));

}

document.querySelector("form").addEventListener("submit", function(event) { 
    event.preventDefault();
    
    let city = document.querySelector('#inputcity').value;

    apicall(city)
});

